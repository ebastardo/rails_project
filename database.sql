-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Farmacia
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Farmacia
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Farmacia` DEFAULT CHARACTER SET utf8 ;
USE `Farmacia` ;

-- -----------------------------------------------------
-- Table `Farmacia`.`Bodega`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Farmacia`.`Bodega` (
  `numero` INT(11) NOT NULL,
  `posicion` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`numero`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Farmacia`.`Categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Farmacia`.`Categoria` (
  `codigo` INT(11) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `Farmacia`.`Empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Farmacia`.`Empleado` (
  `codigo` INT(11) NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  `contrasena` VARCHAR(100) NOT NULL,
  `posicion` VARCHAR(45) NOT NULL,
  `sueldo` FLOAT NOT NULL,
  PRIMARY KEY (`codigo`)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Farmacia`.`Factura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Farmacia`.`Factura` (
  `numero` INT(11) NOT NULL AUTO_INCREMENT,
  `fecha` DATETIME NOT NULL,
  `nombre_cliente` VARCHAR(50) NOT NULL,
  `cod_empleado` INT(11) NULL NOT NULL,
  `tipo_pago` ENUM('Efectivo','Tarjeta','T-Pago') NOT NULL,
  `total` FLOAT NOT NULL,
  PRIMARY KEY (`numero`),
  CONSTRAINT `factura_empleado`
    FOREIGN KEY (`cod_empleado`)
    REFERENCES `Farmacia`.`Empleado` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Farmacia`.`Producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Farmacia`.`Producto` (
  `codigo` INT(11) NOT NULL,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `descripcion` TEXT NULL DEFAULT NULL,
  `costo` FLOAT NULL DEFAULT NULL,
  `precio` FLOAT NULL DEFAULT NULL,
  `stock` INT(11) NULL DEFAULT NULL,
  `cod_categoria` INT(11) NULL DEFAULT NULL,
  `num_bodega` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `producto_categoria_idx` (`cod_categoria` ASC),
  INDEX `producto_bodega_idx` (`num_bodega` ASC),
  CONSTRAINT `producto_bodega`
    FOREIGN KEY (`num_bodega`)
    REFERENCES `Farmacia`.`Bodega` (`numero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `producto_categoria`
    FOREIGN KEY (`cod_categoria`)
    REFERENCES `Farmacia`.`Categoria` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Farmacia`.`Factura_Producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Farmacia`.`Factura_Producto` (
  `cantidad` INT(11) NULL DEFAULT NULL,
  `precio` FLOAT NULL DEFAULT NULL,
  `num_factura` INT(11) NULL DEFAULT NULL,
  `cod_producto` INT(11) NULL DEFAULT NULL,
  INDEX `fp_producto_idx` (`cod_producto` ASC),
  INDEX `fp_factura_idx` (`num_factura` ASC),
  CONSTRAINT `fp_factura`
    FOREIGN KEY (`num_factura`)
    REFERENCES `Farmacia`.`Factura` (`numero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fp_producto`
    FOREIGN KEY (`cod_producto`)
    REFERENCES `Farmacia`.`Producto` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `Farmacia`.`Reporte`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Farmacia`.`Reporte` (
  `numero` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `descripcion` TEXT NULL DEFAULT NULL,
  `date` DATETIME NULL DEFAULT NULL,
  `data` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`numero`)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Farmacia`.`Venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Farmacia`.`Venta` (
  `numero` INT(11) NOT NULL AUTO_INCREMENT,
  `cod_empleado` INT(11) NOT NULL,
  `num_factura` INT(11) NOT NULL,
  `fecha` DATETIME NOT NULL,
  `nombre_cliente` varchar(50) NOT NULL,
  PRIMARY KEY (`numero`),
  INDEX `venta_vendedor_idx` (`cod_empleado` ASC),
  INDEX `venta_factura_idx` (`num_factura` ASC),
  CONSTRAINT `venta_empleado`
    FOREIGN KEY (`cod_empleado`)
    REFERENCES `Farmacia`.`Empleado` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `venta_factura`
    FOREIGN KEY (`num_factura`)
    REFERENCES `Farmacia`.`Factura` (`numero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;