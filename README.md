# [Farmacia Farmax](http://google.com)

# Descargar Dependencias

```
bundle install
```

# Iniciar App

```
rails server
```

# Generate Models

```
rails generate scaffold <<entity name>> <<attr1>>:<<attr1 typ1>> <<attr2>>:<<attr2 type2>> <<attrn>>:<<attrn typen>>
```

# Update Database

```
rake db:migrate
```