// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require cocoon
angular.module('ventas', [])
    .controller('factura', ['$scope', '$http', function ($scope, $http) {
        $scope.productos = [];
        $scope.venta = [];


        $scope.init_productos = function () {
            $http.get("/productos.json").success(function (data) {
                $scope.productos = data;
            });
        };

        $scope.productos_validos = function () {

            var validos = [];

            for (var n in $scope.productos) {
                var valid = true;

                if ($scope.productos[n].stock <= 0) {
                    valid = false;
                }
                for (var v in $scope.venta) {
                    if ($scope.venta[v].id === $scope.productos[n].id) {
                        valid = false;
                    }
                }

                if (valid) {
                    validos.push($scope.productos[n]);
                }
            }

            return validos;
        };

        $scope.agregar_producto = function (id) {
            for (var n in $scope.productos) {
                if ($scope.productos[n].id === id) {
                    var obj = JSON.parse(JSON.stringify($scope.productos[n]))
                    obj.cantidad = 1;
                    $scope.venta.push(obj);
                }
            }
        };

        $scope.remover_producto = function (id) {
            var new_venta = [];
            for (var n in $scope.venta) {
                if ($scope.venta[n].id != id) {
                    new_venta.push($scope.venta[n]);
                }
            }
            $scope.venta = new_venta;
        };

        $scope.calcular = function () {
            var ITBIS = 0.18;

            var subtotal = 0;
            var impuesto = 0;
            var total = 0;

            for (var n in $scope.venta) {
                subtotal += $scope.venta[n].precio * $scope.venta[n].cantidad;
            }

            return {
                subtotal: $scope.redondear(subtotal),
                impuesto: $scope.redondear(subtotal * ITBIS),
                total: $scope.redondear(subtotal + (subtotal * ITBIS))
            }
        };

        $scope.redondear = function(valor){
            return parseFloat((valor).toFixed(2));
        };

    }]);


function imprimir_factura(empleado, cliente, pago, fecha) {
    var contents = $("#factura").html();
    var frame1 = $('<iframe />');
    frame1[0].name = "frame1";
    frame1.css({"position": "absolute", "top": "-1000000px"});
    $("body").append(frame1);
    var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
    frameDoc.document.open();
    //Create a new HTML document.
    frameDoc.document.write('<html><head><title>Factura Farmax</title>');
    frameDoc.document.write('</head><body>');
    //Append the external CSS file.
    frameDoc.document.write('<link rel="stylesheet" href="/css/bootstrap.min.css">');
    //Append the DIV contents.
    frameDoc.document.write('<img src="/img/logo.png" style="width: 100%">');
    frameDoc.document.write('<div><table class="table"><thead><th>Empleado</th><th>Cliente</th><th>Tipo de Pago</th><th>Fecha</th></thead><tbody><tr><td>' + empleado + '</td><td>' + cliente + '</td><td>' + pago + '</td><td>' + fecha + '</td></tr></tbody></table></div>');
    frameDoc.document.write('<hr>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        frame1.remove();
    }, 500);
}