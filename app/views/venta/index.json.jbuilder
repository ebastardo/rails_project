json.array!(@venta) do |ventum|
  json.extract! ventum, :id, :empleado_id, :nombre_cliente, :tipo_pago, :subtotal, :impuesto, :total
  json.url ventum_url(ventum, format: :json)
end
json.array!(@venta_productos) do |v_p|
  json.extract! v_p, :producto, :cantidad, :total
end