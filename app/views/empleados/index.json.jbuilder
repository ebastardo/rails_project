json.array!(@empleados) do |empleado|
  json.extract! empleado, :id, :nombre, :contrasena, :posicion, :sueldo
  json.url empleado_url(empleado, format: :json)
end
