class VentaController < ApplicationController
  before_action :set_ventum, only: [:show, :edit, :update, :destroy]

  # GET /venta
  # GET /venta.json
  def index
    @venta = Ventum.all
    @venta_productos = VentaProducto.joins(:producto).all
  end

  # GET /venta/1
  # GET /venta/1.json
  def show
    # @productos = VentaProducto.find_by_ventum_id(params[:id])
  end

  # GET /venta/new
  def new
    @ventum = Ventum.new
  end

  # GET /venta/1/edit
  def edit
  end

  # POST /venta
  # POST /venta.json
  def create
    productos = JSON.parse ventum_params['productos']
    venta = ventum_params
    venta.delete('productos')
    @ventum = Ventum.new(venta)
    respond_to do |format|
      if @ventum.save
        create_all_relations(@ventum["id"],productos)
        format.html { redirect_to @ventum, notice: 'Venta Creada!' }
        format.json { render :show, status: :created, location: @ventum }
      else
        format.html { render :new }
        format.json { render json: @ventum.errors, status: :unprocessable_entity }
      end
    end
  end


  def create_all_relations(id,array)
    (array).each do |v|
      venta_producto = Hash.new
      venta_producto["producto_id"] = v["id"]
      venta_producto["ventum_id"] = id
      venta_producto["precio"] = v["precio"]
      venta_producto["cantidad"] = v["cantidad"]
      venta_producto["total"] = v["precio"] * v["cantidad"]
      if VentaProducto.new(venta_producto).save
        actualizar_inventario(venta_producto["producto_id"],venta_producto["cantidad"])
      end
    end
  end

  def actualizar_inventario(id,minus)
     producto = Producto.find(id)
     nuevos_params = Hash.new
     nuevos_params["stock"] = producto["stock"] - minus;
     producto.update(nuevos_params)
  end

  # PATCH/PUT /venta/1
  # PATCH/PUT /venta/1.json
  def update
    respond_to do |format|
      if @ventum.update(ventum_params)
        format.html { redirect_to @ventum, notice: 'Venta Actualizada' }
        format.json { render :show, status: :ok, location: @ventum }
      else
        format.html { render :edit }
        format.json { render json: @ventum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /venta/1
  # DELETE /venta/1.json
  def destroy
    @ventum.destroy
    respond_to do |format|
      format.html { redirect_to venta_url, notice: 'Venta Borrada' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ventum
      @ventum = Ventum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ventum_params
      params.require(:ventum).permit(:empleado_id, :nombre_cliente, :tipo_pago, :subtotal, :impuesto, :total, :productos)
    end
end
