class SessionsController < ApplicationController
skip_before_filter :require_login
  def new
  end

  def create
      empleado = Empleado.find_by(nombre: params[:session][:nombre].downcase)
    if empleado && empleado.authenticate(params[:session][:password])
    # Log the user in and redirect to the user's show page.
    
    log_in empleado
    redirect_to root_url
  else
    # Create an error message.
     flash.now[:danger]='Invalid nombre/password combination'
    render 'new'
  end
  end

  def destroy
    log_out
    redirect_to login_path
  end

end
