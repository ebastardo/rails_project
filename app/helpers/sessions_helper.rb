module SessionsHelper
  # Logs in the given user.
  def log_in(empleado)
    session[:empleado_id]=empleado.id
  end


  # Returns the current logged-in user (if any).
   def current_empleado
     @current_empleado ||= Empleado.find_by(id: session[:empleado_id])
   end

    # Returns true if the user is logged in, false otherwise.
    def logged_in?
     !current_empleado.nil?
    end

 # Logs out the current user.
  def log_out
    session.delete(:empleado_id)
    @current_user = nil
  end


end
