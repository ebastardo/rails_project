class CreateVenta < ActiveRecord::Migration
  def change
    create_table :venta do |t|
      t.references :empleado, index: true, foreign_key: true
      t.string :nombre_cliente
      t.string :tipo_pago
      t.float :subtotal
      t.float :impuesto
      t.float :total

      t.timestamps null: false
    end
  end
end
