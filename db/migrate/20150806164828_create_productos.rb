class CreateProductos < ActiveRecord::Migration
  def change
    create_table :productos do |t|
      t.string :nombre
      t.string :descripcion
      t.float :costo
      t.float :precio
      t.integer :stock
      t.string :categoria
      t.string :bodega

      t.timestamps null: false
    end
  end
end
