class CreateEmpleados < ActiveRecord::Migration
  def change
    create_table :empleados do |t|
      t.string :nombre
      t.string :posicion
      t.decimal :sueldo

      t.timestamps null: false
    end
  end
end
