class CreateVentaProductos < ActiveRecord::Migration
  def change
    create_table :venta_productos do |t|
      t.references :producto, index: true, foreign_key: true
      t.references :ventum, index: true, foreign_key: true
      t.float :precio
      t.integer :cantidad
      t.float :total

      t.timestamps null: false
    end
  end
end
