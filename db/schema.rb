# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150809161112) do

  create_table "empleados", force: :cascade do |t|
    t.string   "nombre",          limit: 255
    t.string   "posicion",        limit: 255
    t.decimal  "sueldo",                      precision: 10
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "password_digest", limit: 255
  end

  create_table "productos", force: :cascade do |t|
    t.string   "nombre",      limit: 255
    t.string   "descripcion", limit: 255
    t.float    "costo",       limit: 24
    t.float    "precio",      limit: 24
    t.integer  "stock",       limit: 4
    t.string   "categoria",   limit: 255
    t.string   "bodega",      limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "venta", force: :cascade do |t|
    t.integer  "empleado_id",    limit: 4
    t.string   "nombre_cliente", limit: 255
    t.string   "tipo_pago",      limit: 255
    t.float    "subtotal",       limit: 24
    t.float    "impuesto",       limit: 24
    t.float    "total",          limit: 24
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "venta", ["empleado_id"], name: "index_venta_on_empleado_id", using: :btree

  create_table "venta_productos", force: :cascade do |t|
    t.integer  "producto_id", limit: 4
    t.integer  "ventum_id",   limit: 4
    t.float    "precio",      limit: 24
    t.integer  "cantidad",    limit: 4
    t.float    "total",       limit: 24
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "venta_productos", ["producto_id"], name: "index_venta_productos_on_producto_id", using: :btree
  add_index "venta_productos", ["ventum_id"], name: "index_venta_productos_on_ventum_id", using: :btree

  add_foreign_key "venta", "empleados"
  add_foreign_key "venta_productos", "productos"
  add_foreign_key "venta_productos", "venta"
end
